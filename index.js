const express = require('express')
const bodyParser = require('body-parser')
var array = require('lodash/array')
const app = express()

app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: false }))

app.get('/base', (req, res) => {
  const ans = array.sortedLastIndexBy([4.1, 6.1, 6.4], 6.6, Math.floor)
  return res.status(200).json({ data: ans })
})

app.get('/invalid', (req, res) => {
  const ans = array.tail([1, 2, 3])
  return res.json({ data: ans })
})

app.get('/', (req, res) => {
  const ans = array.sortedLastIndexBy([4.1, 6.1, 6.4], 6.6, Math.floor)
  return res.status(200).json({ data: ans })
})

app.post('/', (req, res) => {
  const { name } = req.body
  if (!name || name === undefined) {
    res.sendStatus(400)
  } else {
    res.json({ input: name })
  }
})

// console.log(
//   [1, 2, 3, 4, 5, 6, 7, 8, 9, 10].map((val) => val * 2),
//   ' multiple by 2'
// )
// Cheking file
// sss
app.listen(3000, () => {
  console.log('Server is up on port 3000')
})

module.exports = { app }
